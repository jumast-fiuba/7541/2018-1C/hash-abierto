#include <stdlib.h>

/* ******************************************************************
 *                        Función de hash
 * *****************************************************************/

/*
 * Algorithms in C - Parts 1-4
 * Third Edition
 * Robert Sedgewick
 * 1998
 *
 * */
size_t fhash(const char *clave, size_t M){
    size_t h = 0;
    size_t a = 31415;
    size_t b = 27183;
    for(; *clave != '\0'; clave++, a = a * b % (M - 1)){
        h = (a * h + (size_t)*clave) % M;
    }
    return h;
}