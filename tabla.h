#ifndef HASHTABLE_TABLA_H
#define HASHTABLE_TABLA_H

#include <stdlib.h>
#include "lista.h"
#include "hash.h" // para hash_destruir_dato_t

typedef struct tabla{
    size_t _tamanio;
    lista_t** _listas;
} tabla_t;

typedef struct tabla_iter{
    size_t _pos_actual;
    lista_t* _lista_actual;
    tabla_t* _tabla;

} tabla_iter_t;

/* ******************************************************************
 *                            tabla_t
 * *****************************************************************/

tabla_t* tabla_crear(size_t tamanio_inicial);
void tabla_destruir(tabla_t* tabla, hash_destruir_dato_t destruir_dato);

/* ******************************************************************
 *                          tabla_iter_t
 * *****************************************************************/

// Avanza el iterador hasta la próxima posición de la tabla para la cual la lista
// correspondiente tiene al menos algún elemento. Si no existe tal posición, avanza
// hasta el final.
//Pre: tabla_iter_al_final(tabla_iter) == false.
bool _avanzar_rec(tabla_iter_t* tabla_iter, size_t posicion);

tabla_iter_t* tabla_iter_crear(tabla_t* tabla);
void tabla_iter_destruir(tabla_iter_t* tabla_iter);
bool tabla_iter_al_final(tabla_iter_t* tabla_iter);
bool tabla_iter_avanzar(tabla_iter_t* tabla_iter);

#endif //HASHTABLE_TABLA_H