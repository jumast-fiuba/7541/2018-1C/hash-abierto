#include <stdlib.h>
#include <string.h>
#include "par_clave_valor.h"

 /* ******************************************************************
 *                       par_clave_valor_t
 * *****************************************************************/

static char* str_copy(const char *clave){
    char* copia = malloc(1 + strlen(clave));
    if(!copia){
        return NULL;
    }
    strcpy(copia, clave);
    return copia;
}

par_clave_valor_t* par_clave_valor_crear(const char *clave, void *valor){
    par_clave_valor_t* par_clave_valor = malloc(sizeof(par_clave_valor_t));
    if(!par_clave_valor){
        return NULL;
    }

    char* copia_clave = str_copy(clave);
    if(!copia_clave){
        return NULL;
    }

    par_clave_valor->_clave = copia_clave;
    par_clave_valor->_valor = valor;
    return par_clave_valor;
}

void par_clave_valor_destruir(par_clave_valor_t* par, hash_destruir_dato_t destruir_dato){
    free(par->_clave);
    if(destruir_dato){
        destruir_dato(par->_valor);
    }
    free(par);
}