#ifndef HASHTABLE_PAR_CLAVE_VALOR_H
#define HASHTABLE_PAR_CLAVE_VALOR_H

#include "hash.h" // para hash_destruir_dato_t

typedef struct {
    char* _clave;
    void* _valor;
} par_clave_valor_t;

par_clave_valor_t* par_clave_valor_crear(const char *clave, void *valor);
void par_clave_valor_destruir(par_clave_valor_t* par, hash_destruir_dato_t destruir_dato);

#endif //HASHTABLE_PAR_CLAVE_VALOR_H