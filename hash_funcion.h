#ifndef HASHTABLE_HASH_FUNCION_H
#define HASHTABLE_HASH_FUNCION_H

#include <stdlib.h>

size_t fhash(const char* clave, size_t M);

#endif //HASHTABLE_HASH_FUNCION_H