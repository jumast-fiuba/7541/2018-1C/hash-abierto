CC = gcc
CFLAGS = -g -std=c99 -Wall -Wconversion -Wno-sign-conversion -Werror -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = pruebas

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

HASH_OBJECTS = busqueda_iter.o hash.o hash_funcion.o lista.o main.o pruebas_catedra.o par_clave_valor.o tabla.o testing.o
build: $(HASH_OBJECTS) 
	$(CC) $(CFLAGS) -o $(EXEC) $(HASH_OBJECTS)

run: build
	./$(EXEC)
	$(MAKE) clean

val: build
	valgrind $(VALFLAGS) ./$(EXEC)
	$(MAKE) clean

time: build
	./tiempos_volumen.sh ./$(EXEC)
	$(MAKE) clean

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

ENTREGA = busqueda_iter.c busqueda_iter.h
ENTREGA += hash.c hash.h
ENTREGA += hash_funcion.c hash_funcion.h
ENTREGA += lista.c lista.h
ENTREGA += par_clave_valor.c par_clave_valor.h
ENTREGA += tabla.c tabla.h
ENTREGA += Makefile
ZIPNAME = hash_entrega.zip
zip:
	zip $(ZIPNAME) $(ENTREGA)




