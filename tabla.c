#include "tabla.h"
#include "lista.h"
#include "hash.h" // para hash_destruir_dato_t
#include "par_clave_valor.h"

/* ******************************************************************
 *                              tabla_t
 * *****************************************************************/
static lista_t** _tabla_crear_listas(size_t cantidad_de_listas){
    lista_t** listas = malloc((cantidad_de_listas) * sizeof(lista_t*));
    if(!listas) return NULL;

    for(size_t i = 0; i < cantidad_de_listas; i++){
        lista_t* lista = lista_crear();
        if(!lista){
            for(size_t j = 0; j < i;j++) {
                lista_destruir(listas[j], NULL);
            }
            free(listas);
            return NULL;
        }
        listas[i] = lista;
    }

    return listas;
}

tabla_t* tabla_crear(size_t tamanio_inicial){
    tabla_t* tabla = malloc(sizeof(tabla_t));
    if(!tabla) return NULL;

    lista_t** listas = _tabla_crear_listas(tamanio_inicial);
    if(!listas){
        free(tabla);
        return NULL;
    }

    tabla->_tamanio = tamanio_inicial;
    tabla->_listas = listas;
    return tabla;
}

void tabla_destruir(tabla_t* tabla, hash_destruir_dato_t destruir_dato){
    for (size_t i = 0; i < tabla->_tamanio; i++) {
        lista_t* lista = tabla->_listas[i];

        // destruir lista
        while(!lista_esta_vacia(lista)){
            par_clave_valor_t* par_clave_valor = lista_borrar_primero(lista);
            par_clave_valor_destruir(par_clave_valor, destruir_dato);
        }
        free(lista);
    }
    free(tabla->_listas);
    free(tabla);
}
// FIN tabla_t

/* ******************************************************************
 *                  Funciones auxiliares - tabla_iter_t
 * *****************************************************************/

// Avanza el iterador hasta la próxima posición de la tabla para la cual la lista
// correspondiente tiene al menos algún elemento. Si no existe tal posición, avanza
// hasta el final.
//Pre: tabla_iter_al_final(tabla_iter) == false.

bool _avanzar_rec(tabla_iter_t* tabla_iter, size_t posicion){
    lista_t** listas = tabla_iter->_tabla->_listas;
    size_t tamanio = tabla_iter->_tabla->_tamanio;

    if(posicion == tamanio){
        tabla_iter->_pos_actual = posicion;
        tabla_iter->_lista_actual = NULL;
        return false;
    }

    lista_t* lista = listas[posicion];
    if(!lista_esta_vacia(lista)){
        tabla_iter->_pos_actual = posicion;
        tabla_iter->_lista_actual = lista;
        return true;
    }

    return _avanzar_rec(tabla_iter, ++posicion);
}

tabla_iter_t* tabla_iter_crear(tabla_t* tabla){
    tabla_iter_t* tabla_iter = malloc(sizeof(tabla_iter_t));
    if(!tabla_iter) return NULL;

    size_t posicion_inicial = 0;
    tabla_iter->_tabla = tabla;
    tabla_iter->_pos_actual = posicion_inicial;
    tabla_iter->_lista_actual = tabla->_listas[posicion_inicial];

    if(!lista_esta_vacia(tabla_iter->_lista_actual)){
        return tabla_iter;
    }
    else{
        _avanzar_rec(tabla_iter, 1);
    }

    return tabla_iter;
}

void tabla_iter_destruir(tabla_iter_t* tabla_iter){
    free(tabla_iter);
}

bool tabla_iter_al_final(tabla_iter_t* tabla_iter){
    return tabla_iter->_tabla->_tamanio == tabla_iter->_pos_actual;
}

bool tabla_iter_avanzar(tabla_iter_t* tabla_iter){
    if(tabla_iter_al_final(tabla_iter)){
        return false;
    }
    return _avanzar_rec(tabla_iter, tabla_iter->_pos_actual + 1);
}