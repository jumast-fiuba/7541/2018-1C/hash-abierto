#include <string.h>
#include "busqueda_iter.h"
#include "tabla.h"

/* ******************************************************************
 *                      busqueda_iter_t
 * *****************************************************************/
/*busqueda_iter_t* busqueda_iter_buscar(const hash_t *hash, const char *clave);
Implementada en hash.c porque necesita acceder a su extructura interna.*/

void busqueda_iter_destruir(busqueda_iter_t *busqueda){
    lista_iter_destruir(busqueda->lista_iter);
    free(busqueda);
}

void* busqueda_iter_ver_actual(busqueda_iter_t* busqueda_iter){
    return busqueda_iter->par_clave_valor->_valor;
}

void busqueda_iter_reemplazar(busqueda_iter_t* busqueda_iter, void* valor_nuevo, hash_destruir_dato_t destruir_dato){
    if(destruir_dato){
        destruir_dato(busqueda_iter->par_clave_valor->_valor);
    }
    busqueda_iter->par_clave_valor->_valor = valor_nuevo;
}

bool busqueda_iter_insertar(busqueda_iter_t* busqueda_iter, const char* clave, void* valor){
    par_clave_valor_t* par = par_clave_valor_crear(clave, valor);
    if(par == NULL) return false;
    return lista_iter_insertar(busqueda_iter->lista_iter, par);
}

void* busqueda_iter_borrar(busqueda_iter_t *busqueda){
    par_clave_valor_t* par = lista_iter_borrar(busqueda->lista_iter);
    void* valor = NULL;
    if(par){
        valor = par->_valor;
        par_clave_valor_destruir(par, NULL);
        busqueda->par_clave_valor = NULL;
    }
    return valor;
}