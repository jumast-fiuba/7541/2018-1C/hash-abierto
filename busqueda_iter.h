#ifndef HASHTABLE_BUSQUEDA_ITER_H
#define HASHTABLE_BUSQUEDA_ITER_H

#include <stdbool.h>
#include "lista.h"
#include "par_clave_valor.h"

typedef struct {
    bool encontrado;
    par_clave_valor_t* par_clave_valor;
    lista_iter_t* lista_iter;
} busqueda_iter_t;

busqueda_iter_t* busqueda_iter_buscar(const hash_t *hash, const char *clave);
void busqueda_iter_destruir(busqueda_iter_t *busqueda);
void* busqueda_iter_ver_actual(busqueda_iter_t* busqueda_iter);
void busqueda_iter_reemplazar(busqueda_iter_t* busqueda_iter, void* valor_nuevo, hash_destruir_dato_t destruir_dato);
bool busqueda_iter_insertar(busqueda_iter_t* busqueda_iter, const char* clave, void* valor);
void* busqueda_iter_borrar(busqueda_iter_t *busqueda);

#endif //HASHTABLE_BUSQUEDA_ITER_H