#include <string.h>
#include "lista.h"
#include "par_clave_valor.h"
#include "hash.h"
#include "tabla.h"
#include "busqueda_iter.h"
#include "hash_funcion.h"

#define TAMANIO_INICIAL_DE_LA_TABLA 4
#define MAXIMO_FACTOR_DE_CARGA 2
#define MINIMO_FACTOR_DE_CARGA 0.25
#define FACTOR_REDIMENSIONAMIENTO_AGRANDAR 2
#define FACTOR_REDIMENSIONAMIENTO_ACHICAR 2

struct hash {
    tabla_t* _tabla;
    size_t _cantidad;
    hash_destruir_dato_t _destruir_dato;
};

struct hash_iter{
    tabla_iter_t* _tabla_iter;
    lista_iter_t* _lista_iter;
};

/* ******************************************************************
 *               Declaración de funciones auxiliares
 * *****************************************************************/
static void destruir_dato_dummy(void* valor);
static float _factor_de_carga(hash_t *hash);
static hash_t* _hash_crear(size_t tamanio_inicial_de_la_tabla);
static void _redimensionar(hash_t *hash, size_t tamanio_nuevo);
static void hash_decrementar_capacidad(hash_t* hash);
static void hash_incrementar_capacidad(hash_t* hash);

// Implementada en hash.c porque necesita acceder a la estructura interna de struct hash.*/
busqueda_iter_t* busqueda_iter_buscar(const hash_t *hash, const char *clave){
    busqueda_iter_t* busqueda_iter = malloc(sizeof(busqueda_iter_t));
    if(busqueda_iter == NULL) return NULL;

    size_t lista_pos = fhash(clave, hash->_tabla->_tamanio);
    lista_t* lista = hash->_tabla->_listas[lista_pos];

    lista_iter_t* lista_iter = lista_iter_crear(lista);
    if(!lista_iter) return NULL;

    busqueda_iter->encontrado = false;
    busqueda_iter->par_clave_valor = NULL;
    busqueda_iter->lista_iter = lista_iter;

    while(!lista_iter_al_final(lista_iter)){
        par_clave_valor_t* par = lista_iter_ver_actual(lista_iter);
        if(strcmp(clave, par->_clave) == 0){
            busqueda_iter->encontrado = true;
            busqueda_iter->par_clave_valor = par;
            break;
        }
        lista_iter_avanzar(lista_iter);
    }
    return busqueda_iter;
}

/* ******************************************************************
 *                       HASH - PRIMITIVAS
 * *****************************************************************/
hash_t* hash_crear(hash_destruir_dato_t destruir_dato){
    hash_t* hash = _hash_crear(TAMANIO_INICIAL_DE_LA_TABLA);
    if(!hash) return NULL;

    if(destruir_dato){
        hash->_destruir_dato = destruir_dato;
    }
    else{
        hash->_destruir_dato = destruir_dato_dummy;
    }
    return hash;
}

void hash_destruir(hash_t* hash){
    tabla_destruir(hash->_tabla, hash->_destruir_dato);
    free(hash);
}

bool hash_guardar(hash_t* hash, const char *clave, void* valor){
    busqueda_iter_t* busqueda_iter = busqueda_iter_buscar(hash, clave);
    if(busqueda_iter == NULL) return false;

    if(busqueda_iter->encontrado == true){
        busqueda_iter_reemplazar(busqueda_iter, valor, hash->_destruir_dato);
        busqueda_iter_destruir(busqueda_iter);
        return true;
    }

    bool guardado = busqueda_iter_insertar(busqueda_iter, clave, valor);
    busqueda_iter_destruir(busqueda_iter);
    if(!guardado) return false;

    hash_incrementar_capacidad(hash);
    return guardado;
}

void* hash_borrar(hash_t* hash, const char *clave){
    busqueda_iter_t* busqueda_iter = busqueda_iter_buscar(hash, clave);
    if(busqueda_iter == NULL) return NULL;

    if(busqueda_iter->encontrado == false){
        busqueda_iter_destruir(busqueda_iter);
        return NULL;
    }

    void* valor = busqueda_iter_borrar(busqueda_iter);
    busqueda_iter_destruir(busqueda_iter);

    hash_decrementar_capacidad(hash);
    return valor;
}

void* hash_obtener(const hash_t* hash, const char* clave){
    busqueda_iter_t* busqueda_iter = busqueda_iter_buscar(hash, clave);
    if(busqueda_iter == NULL) return NULL;

    if(busqueda_iter->encontrado == false){
        busqueda_iter_destruir(busqueda_iter);
        return NULL;
    }

    void* valor = busqueda_iter_ver_actual(busqueda_iter);
    busqueda_iter_destruir(busqueda_iter);
    return valor;
}

bool hash_pertenece(const hash_t* hash, const char* clave){
    busqueda_iter_t* busqueda_iter = busqueda_iter_buscar(hash, clave);
    if(busqueda_iter == NULL) return false;

    bool encontrado = busqueda_iter->encontrado;
    busqueda_iter_destruir(busqueda_iter);
    return encontrado;
}

size_t hash_cantidad(const hash_t* hash){
    return hash->_cantidad;
}
// FIN PRIMITIVAS HASH

/* ******************************************************************
 *                     HASH_ITER - PRIMITIVAS
 * *****************************************************************/
hash_iter_t* hash_iter_crear(const hash_t* hash){
    hash_iter_t* hash_iter = malloc(sizeof(hash_iter_t));
    if(hash_iter == NULL) return NULL;
    hash_iter->_tabla_iter = NULL;
    hash_iter->_lista_iter = NULL;

    tabla_iter_t* tabla_iter = tabla_iter_crear(hash->_tabla);
    if(tabla_iter == NULL){
        free(hash_iter);
        return NULL;
    }
    hash_iter->_tabla_iter = tabla_iter;

    if(tabla_iter_al_final(tabla_iter)){
        return hash_iter;
    }

    lista_iter_t* lista_iter = lista_iter_crear(tabla_iter->_lista_actual);
    if(!lista_iter){
        free(hash_iter);
        tabla_iter_destruir(tabla_iter);
        return NULL;
    }
    hash_iter->_lista_iter = lista_iter;

    return hash_iter;
}

bool hash_iter_avanzar(hash_iter_t* hash_iter){
    if(hash_iter_al_final(hash_iter)) return false;

    lista_iter_t* lista_iter = hash_iter->_lista_iter;
    bool lista_iter_avanzo = lista_iter_avanzar(lista_iter);
    bool lista_iter_final = lista_iter_al_final(lista_iter);
    if(lista_iter_avanzo && !lista_iter_final) return true;

    tabla_iter_t* tabla_iter = hash_iter->_tabla_iter;
    bool tabla_iter_avanzo = tabla_iter_avanzar(tabla_iter);
    bool tabla_iter_final = tabla_iter_al_final(tabla_iter);
    if(!tabla_iter_avanzo || tabla_iter_final) return false;

    lista_iter_destruir(hash_iter->_lista_iter);
    lista_iter_t* lista_iter_nuevo = lista_iter_crear(hash_iter->_tabla_iter->_lista_actual);
    if(!lista_iter_nuevo) return false;
    hash_iter->_lista_iter = lista_iter_nuevo;
    return true;
}

const char* hash_iter_ver_actual(const hash_iter_t *iter){
    if(hash_iter_al_final(iter)){
        return NULL;
    }

    par_clave_valor_t* par_clave_valor = lista_iter_ver_actual(iter->_lista_iter);
    return par_clave_valor->_clave;
}

bool hash_iter_al_final(const hash_iter_t *iter){
    return tabla_iter_al_final(iter->_tabla_iter);
}

void hash_iter_destruir(hash_iter_t* iter){
    tabla_iter_destruir(iter->_tabla_iter);
    lista_iter_destruir(iter->_lista_iter);
    free(iter);
}
// FIN PRIMITIVAS HASH_ITER


/* ******************************************************************
 *                   Funciones auxiliares
 * *****************************************************************/
static void destruir_dato_dummy(void* valor){
    // hace nada
    ;
}

static void hash_decrementar_capacidad(hash_t* hash){
    hash->_cantidad--;

    // achicar?
    if(_factor_de_carga(hash) >= MINIMO_FACTOR_DE_CARGA) return;

    // achicar
    size_t tamanio_actual = hash->_tabla->_tamanio;
    _redimensionar(hash, tamanio_actual / FACTOR_REDIMENSIONAMIENTO_ACHICAR);
}

static void hash_incrementar_capacidad(hash_t* hash){
    hash->_cantidad++;

    //expandir?
    if(_factor_de_carga(hash) <= MAXIMO_FACTOR_DE_CARGA) return;

    // expandir
    size_t tamanio_actual = hash->_tabla->_tamanio;
    _redimensionar(hash, tamanio_actual * FACTOR_REDIMENSIONAMIENTO_AGRANDAR);

}

static float _factor_de_carga(hash_t *hash){
    return (float)hash->_cantidad / (float)hash->_tabla->_tamanio;
}

static void _redimensionar(hash_t *hash, size_t tamanio_nuevo){
    hash_t* hash_aux = _hash_crear(tamanio_nuevo);
    if(!hash_aux) return;

    for(size_t i = 0; i < hash->_tabla->_tamanio; i++){
        lista_t* lista = hash->_tabla->_listas[i];
        while(!lista_esta_vacia(lista)){
            par_clave_valor_t* par_clave_valor =  lista_borrar_primero(lista);
            const char* clave = par_clave_valor->_clave;

            size_t pos = fhash(clave, tamanio_nuevo);
            lista_t* lista2 = hash_aux->_tabla->_listas[pos];
            lista_insertar_primero(lista2, par_clave_valor);
        }
        lista_destruir(lista, NULL);
    }
    free(hash->_tabla->_listas);
    free(hash->_tabla);
    hash->_tabla = hash_aux->_tabla;
    free(hash_aux);
}

static hash_t* _hash_crear(size_t tamanio_inicial_de_la_tabla){
    hash_t* hash = malloc(sizeof(hash_t));
    if(!hash) return NULL;

    tabla_t* tabla = tabla_crear(tamanio_inicial_de_la_tabla);
    if(!tabla){
        free(hash);
        return NULL;
    }

    hash->_tabla = tabla;
    hash->_cantidad = 0;

    return hash;
}
// FIN FUNCIONES AUXILIARES